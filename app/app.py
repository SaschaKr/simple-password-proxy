from flask import request, Response, Flask, send_from_directory, abort
from dotenv import load_dotenv
from hashlib import sha256

import requests 
import os
import random

app = Flask(__name__, static_url_path='/templates')

load_dotenv()


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def hello_world(path=''):
    session_id = request.cookies.get('SESSION_ID')
    if session_id == create_session_id():
        return proxy_request(path)
    else: 
        return send_from_directory('templates', 'login.html')


@app.route('/authorize', methods=['POST'])
def authorize():
    password = request.json.get('password');
    if password == expectedPassword: 
        response = Response(status = 200)
        response.set_cookie('SESSION_ID', create_session_id())
        return response

    return Response(status = 401)

def proxy_request(path): 
    res = requests.request(
        method          = request.method,
        url             = request.url.replace(request.host_url, f'{redirect}/'),
        headers         = {k:v for k,v in request.headers if k.lower() == 'host'},
        data            = request.get_data(),
        cookies         = request.cookies,
        allow_redirects = True,
    )

    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers          = [
        (k,v) for k,v in res.raw.headers.items()
        if k.lower() not in excluded_headers
    ]

    response = Response(res.content, res.status_code, headers)
    return response

def fetch_client_ip(): 
    if 'X-Forwarded-For' in request.headers:
        proxy_data = request.headers['X-Forwarded-For']
        ip_list = proxy_data.split(',')
        return ip_list[0]  # first address in list is User IP
    else:
        return request.remote_addr  # For local development

def create_session_id(): 
    id_values = request.headers.get('User-Agent') + fetch_client_ip() + expectedPassword
    return sha256(id_values.encode('utf-8')).hexdigest()

def create_random_password():

    random_password = ''.join([random.choice(random.choice(
        [
        ['a','e','f','g','h','m','n','t','y'],
        ['A','B','E','F','G','H','J','K','L','M','N','Q','R','T','X','Y'],
        ['2','3','4','5','6','7','8','9'],
        ['/','*','+','~','@','#','%','^','&']
        ])) 
        for i in range(24)])

    print("=============================")
    print("= GENERATE RANDOM PASSWORD  =")
    print("=                           =")
    print("= Password                  =")
    print("= " + random_password + "  =")
    print("=                           =")
    print("=============================")

    return random_password

redirect=os.getenv("REDIRECT") or "http://app"
expectedPassword=os.getenv("PASSWORD") or create_random_password()
