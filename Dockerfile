FROM python:3.8-slim-buster

LABEL org.opencontainers.image.authors="SaschaKr"

# use /app as working directory
WORKDIR /app

# copy requirements and install them
COPY app/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# copy app directroy
COPY ./app .

# App will expose PORT 5000
EXPOSE 5000

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]