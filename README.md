# Simple Password Proxy

`Simple Password Proxy` is a small project to password-protect web pages with little effort.

> This is *_not_* an alternative to identity or access management services.

## UI Samples

| Desktop View                                           | Mobile View                                          |
| ------------------------------------------------------ | ---------------------------------------------------- |
| ![Desktop view](.gitlab/images/screenshot-desktop.png) | ![Mobile view](.gitlab/images/screenshot-mobile.png) |


## Sample Configuration

```yml
version: "3.1"
services:
  app:
    image: nginx
    # configure as you want
    # there is no need to expose any port!

  proxy:
    image: registry.gitlab.com/saschakr/simple-password-proxy:latest
    ports: 
      - 8080:5000 # map any port you want
    environment:
       # because we use docker-compose we can simple redirect to http://app where `app` is the service name above.
      - REDIRECT=http://app

      # any password you want
      - PASSWORD=super-duper-password 
```

